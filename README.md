## INSTALL
    - `brew update`
    - `brew install rabbitmq`
    - Add `PATH=$PATH:/usr/local/sbin` to your .bash_profile or .profile
    - `pip install -r requirements.txt`
 
## RUN
    - Start RabbitMq server `rabbitmq-server`
    - python main.py