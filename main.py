from random import randint

from proj.tasks import add, xsum, mul

n = 20
# store results here
results = {}

while n > 0:
    x = randint(0, n)
    y = randint(0, n)

    request1 = add.apply_async((n, x, y), link=[mul.s(x, y)])
    results[n] = request1

    n = n-1


# process results
while len(results) > 0:
    to_remove = []

    for key in results:
        result = results[key]

        if result.ready():
            print('------------------------')
            print(result.get())
            print('Result: ' + str(result.children[0].get()))
            to_remove.append(key)

    for idx in to_remove:
        del results[idx]
