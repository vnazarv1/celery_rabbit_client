from __future__ import absolute_import, unicode_literals
from .celery import app


@app.task
def mul(r, x, y):
    """mul Interface"""
    return

@app.task
def xsum(numbers):
    """xsum Interface"""
    return

@app.task
def add(n, x, y):
    """add Interface"""
    return

