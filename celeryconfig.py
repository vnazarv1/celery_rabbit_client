broker_url = 'amqp://localhost'
result_backend = 'amqp://localhost'

task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']
timezone = 'EST'
enable_utc = True
task_routes = {
    'proj.tasks.add': {'queue': 'some_task_add'},
    'proj.tasks.xsum': {'queue': 'some_task_xsum'},
    'proj.tasks.mul': {'queue': 'some_task_mul'},
}